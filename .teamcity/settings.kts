import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.finishBuildTrigger
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.1"

project {
    description = "This is test project for the espn app"

    buildType(id01releaseParentjob)
    buildType(id01bReleaseAppStore)
    buildType(id01aReleaseEnterprise)

    template(ReleaseTemplate)
}

object id01aReleaseEnterprise : BuildType({
    templates(ReleaseTemplate)
    id("01aReleaseEnterprise")
    name = "01.a. release - enterprise"

    params {
        param("build.script", "script_enterprise.sh")
    }
})

object id01bReleaseAppStore : BuildType({
    templates(ReleaseTemplate)
    id("01bReleaseAppStore")
    name = "01.b. release - app_store"

    params {
        param("build.script", "script_appstore.sh")
    }
})

object id01releaseParentjob : BuildType({
    id("01releaseParentjob")
    name = "01. release parentjob"

    type = BuildTypeSettings.Type.COMPOSITE

    vcs {
        root(DslContext.settingsRoot)

        showDependenciesChanges = true
    }

    triggers {
        vcs {
            triggerRules = "+:*"
            enableQueueOptimization = false
        }
    }
})

object ReleaseTemplate : Template({
    name = "release_template"
    description = "release jobs templates."

    buildNumberPattern = "${id01releaseParentjob.depParamRefs["env.BUILD_NUMBER"]}"

    params {
        param("build.script", "script_jobtype.sh")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            id = "RUNNER_2"
            scriptContent = """
                #!/bin/bash
                
                ./%build.script%
            """.trimIndent()
        }
    }

    triggers {
        finishBuildTrigger {
            id = "TRIGGER_2"
            buildType = "${id01releaseParentjob.id}"
            successfulOnly = true
        }
    }

    dependencies {
        snapshot(id01releaseParentjob) {
            reuseBuilds = ReuseBuilds.ANY
            onDependencyFailure = FailureAction.CANCEL
            onDependencyCancel = FailureAction.CANCEL
        }
    }
})
